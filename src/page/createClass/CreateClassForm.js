import {
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Button, DropdownWithLabel, Header, TextInputWithLabel} from '../widget';
import FetchLoading from '../widget/FetchLoading';
import {COLORS, FontSize, FontType, matrics} from '../../utility';
import ImagePicker from 'react-native-image-crop-picker';
import Toast from 'react-native-toast-message';
import {userContext} from '../../realm';
import {Kelas, User, listKelas} from '../../realm/User';
import {useAuth} from '../../reducer/AuthContextProvider';
import 'react-native-get-random-values';
import {nanoid} from 'nanoid';
const {useRealm, useQuery} = userContext;

const initdata = {
  _id: '',
  className: '',
  deskripsi: '',
  image: '',
  createdBy: '',
  guruId: '',
};

const initsiswa = {
  id: '',
  label: '',
  value: '',
  siswaId: '',
};

const CreateClassForm = ({route, navigation}) => {
  const [dataForm, setDataForm] = useState(initdata);
  const [dataDropdown, setDataDropdown] = useState([]);
  const [dataDropdownTeacher, setDataDropdownTeacher] = useState([]);
  const [fetching, setFetching] = useState(false);
  const realm = useRealm();
  const userDB = useQuery(User);
  const listKelasDB = useQuery(listKelas);
  const AuthContext = useAuth();
  const resultUser = userDB.find(item => item._id === AuthContext.user._id);
  const roleSiswa = userDB.filter(item => item.role === 'Siswa');
  const roleGuru = userDB.filter(item => item.role === 'Guru');
  const [selected, setSelected] = useState([]);

  // console.log('selected', selected);
  console.log('dataDropdown', dataDropdown);
  console.log('dataDropdownTeacher', dataDropdownTeacher);
  // console.log('params', route?.params);
  // console.log('cek dataForm _id', JSON.stringify(dataForm._id));
  // console.log('cek dataForm guruId', JSON.stringify(dataForm.guruId));
  // console.log('cek dataForm className', JSON.stringify(dataForm.className));
  // console.log('cek dataForm createdBy', JSON.stringify(dataForm.createdBy));
  // console.log('cek roleSiswa', roleSiswa);
  // console.log('cek resultGuru', resultGuru);
  // console.log('AuthContext', AuthContext);
  useEffect(() => {
    if (AuthContext.user.role == 'Admin') {
      if (roleGuru.length > 0 && dataDropdownTeacher.length === 0) {
        roleGuru.map(item => {
          let temp = {
            label: item.name,
            value: item._id,
          };
          dataDropdownTeacher.push(temp);
        });
      }
    }
  }, []);

  useEffect(() => {
    if (route.params && route.params.paramsListKelas) {
      // console.log(
      //   'route.params?.paramsListKelas',
      //   JSON.stringify(route.params.paramsListKelas),
      // );
      setDataForm(route.params.paramsListKelas);
      if (roleSiswa.length > 0 && dataDropdown.length === 0) {
        roleSiswa.map(item => {
          let temp = {
            _id: nanoid(),
            label: item.name,
            name: item.name,
            value: item._id,
            siswaId: item._id,
          };
          dataDropdown.push(temp);
        });
      }
      if (route.params.paramsListKelas.listStudent.length > 0) {
        route.params.paramsListKelas.listStudent.map(item => {
          selected.push(item.siswaId);
        });
      }
    } else {
      if (roleSiswa.length > 0 && dataDropdown.length === 0) {
        roleSiswa.map(item => {
          let temp = {
            _id: nanoid(),
            label: item.name,
            name: item.name,
            value: item._id,
            siswaId: item._id,
          };
          dataDropdown.push(temp);
        });
      }
    }
  }, [route.params]);

  // funtion create form classroom
  const doCreateForm = async () => {
    setFetching(true);
    try {
      let dataTemp = dataForm;
      if (AuthContext.user.role !== 'Admin') {
        dataTemp.createdBy = AuthContext.user.name;
        dataTemp.guruId = AuthContext.user._id;
      }

      if ((roleGuru.length === 0) & (AuthContext.user.role === 'Admin')) {
        dataTemp.createdBy = 'Teacher Not Available';
        dataTemp.guruId = AuthContext.user._id;
      }

      const findDropdown = dataDropdownTeacher.find(
        item => item.value === dataForm.guruId,
      );

      let dataSiswa = [];
      selected.length > 0 &&
        selected.map(item => {
          let response = dataDropdown.find(val => val.siswaId === item);
          // console.log('cek response', response)
          let temp = {
            _id: response._id,
            siswaId: response.siswaId,
            name: response.name,
            kelas: dataForm.className,
          };
          dataSiswa.push(temp);
        });
      dataTemp._id = nanoid();
      // console.log('masuk', resultGuru);
      console.log('dataArray', JSON.stringify(dataTemp));
      realm.write(() => {
        if (AuthContext.user.role === 'Admin') {
          if (roleGuru.length === 0) {
            realm.create(
              'listKelas',
              {
                ...dataTemp,
                listStudent: dataSiswa,
              },
              'modified',
            );
          } else {
            realm.create(
              'listKelas',
              {
                ...dataTemp,
                createdBy: findDropdown.label,
                listStudent: dataSiswa,
              },
              'modified',
            );
          }
        } else {
          realm.create(
            'listKelas',
            {...dataTemp, listStudent: dataSiswa},
            'modified',
          );
        }
      });
      Toast.show({
        type: 'success',
        position: 'bottom',
        text1: 'Buat Kelas Berhasil',
        visibilityTime: 1000,
      });
      setTimeout(() => {
        setFetching(false);
        navigation.goBack();
      }, 1000);
    } catch (error) {
      setFetching(false);
      console.error(error);
    }
  };

  // function update data classroom
  const doEditForm = async () => {
    setFetching(true);
    try {
      const findDropdown = dataDropdownTeacher.find(
        item => item.value === dataForm.guruId,
      );
      let dataSiswa = [];
      selected.length > 0 &&
        selected.map(item => {
          let response = dataDropdown.find(val => val.siswaId === item);
          // console.log('cek response', response)
          let temp = {
            _id: response._id,
            siswaId: response.siswaId,
            name: response.name,
            kelas: dataForm.className,
          };
          dataSiswa.push(temp);
        });
      // console.log('findDropdown======>', JSON.stringify(findDropdown));
      realm.write(() => {
        if (AuthContext.user.role == 'Admin') {
          if (roleGuru.length === 0) {
            realm.create(
              'listKelas',
              {
                ...dataForm,
                listStudent: dataSiswa,
              },
              'modified',
            );
          } else {
            realm.create(
              'listKelas',
              {
                ...dataForm,
                guruId: findDropdown.value,
                createdBy: findDropdown.label,
                listStudent: dataSiswa,
              },
              'modified',
            );
          }
        } else {
          realm.create(
            'listKelas',
            {
              ...dataForm,
              listStudent: dataSiswa,
            },
            'modified',
          );
        }
      });
      Toast.show({
        type: 'success',
        position: 'bottom',
        text1: 'Update Kelas Berhasil',
        visibilityTime: 1000,
      });
      setTimeout(() => {
        setFetching(false);
        navigation.pop(2);
      }, 1000);
    } catch (error) {
      setFetching(false);
      console.error(error);
    }
  };

  // pick a image from library phone
  const doPickImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      // cropping: true,
      includeBase64: true,
      // includeExif: true,
    }).then(image => {
      console.log('height--->', image.height, 'width--->', image.width);
      console.log('cek image--->', image);
      if (image.height > image.width) {
        Toast.show({
          type: 'error',
          position: 'bottom',
          text1: 'Upload Failed',
          text2: 'Picture must be landscape',
          visibilityTime: 1000,
        });
        ImagePicker.clean()
          .then(() => {
            console.log('removed all tmp images from tmp directory');
          })
          .catch(e => {
            alert(e);
          });
      } else {
        setDataForm({
          ...dataForm,
          image: `data:${image.mime};base64,${image.data}`,
          // image: {
          //   uri: image.path,
          //   width: image.width,
          //   height: image.height,
          //   mime: image.mime,
          // },
        });
        // setDataForm({ ...dataForm, image: image });
      }
    });
  };

  return (
    <View style={{flex: 1}}>
      <Header screen={route.name} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{flex: 2.5, width: matrics.width * 0.8, alignSelf: 'center'}}>
        <View style={{height: 50}} />
        <TextInputWithLabel
          label={'Nama Kelas'}
          placeholder={'Masukkan nama kelas'}
          placeholderTextColor={COLORS.grayText}
          style={{width: '100%'}}
          value={dataForm.className}
          onChangeText={className => setDataForm({...dataForm, className})}
        />
        <View style={{height: 10}} />
        <TextInputWithLabel
          label={'Deskripsi'}
          placeholder={'Masukkan deskripsi'}
          placeholderTextColor={COLORS.grayText}
          style={{width: '100%'}}
          multiline={true}
          value={dataForm.deskripsi}
          onChangeText={deskripsi => setDataForm({...dataForm, deskripsi})}
        />
        <View style={{height: 10}} />
        <Text style={styles.textLabel}>{'Image Classroom'}</Text>
        <View style={{height: 10}} />
        <Pressable onPress={doPickImage} style={styles.upload}>
          <Image
            source={
              dataForm.image
                ? {
                    uri: dataForm.image,
                  }
                : require('../../assets/images/no-image-icon.png')
            }
            resizeMode="contain"
            style={{width: '100%', height: '100%'}}
          />
        </Pressable>
        <View style={{height: 20}} />
        {roleSiswa.length > 0 ? (
          <>
            <DropdownWithLabel
              label={'Pilih Siswa'}
              styleDropdown={{width: '100%'}}
              style={{width: '100%'}}
              data={dataDropdown}
              value={selected}
              multiSelect={true}
              setValue={item => {
                setSelected(item);
              }}
            />
            <View style={{height: 20}} />
          </>
        ) : null}
        {AuthContext.user.role === 'Admin' && roleGuru.length > 0 && (
          <>
            <DropdownWithLabel
              label={'Pilih Guru'}
              styleDropdown={{width: '100%'}}
              style={{width: '100%'}}
              data={dataDropdownTeacher}
              value={dataForm.guruId}
              setValue={guruId => {
                setDataForm({...dataForm, guruId});
              }}
            />
            <View style={{height: 20}} />
          </>
        )}
        <Button
          label={route?.params ? 'Update Kelas' : 'Buat Kelas'}
          style={{minWidth: 150}}
          disabled={
            dataForm.email != '' &&
            dataForm.password != '' &&
            dataForm.telp != '' &&
            dataForm.password2 != '' &&
            dataForm.nama != ''
              ? false
              : true
          }
          onPress={route?.params ? doEditForm : doCreateForm}
        />
        <View style={{height: 20}} />
      </ScrollView>
      <FetchLoading visible={fetching} />
    </View>
  );
};

export default CreateClassForm;

const styles = StyleSheet.create({
  textLabel: {
    fontFamily: FontType.poppinsMedium,
    textAlign: 'left',
    fontSize: FontSize.font14,
    color: COLORS.green,
  },
  upload: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: FontSize.font12,
    height: 200,
    padding: 10,
    lineHeight: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
