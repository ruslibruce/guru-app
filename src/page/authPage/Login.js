import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {userContext} from '../../realm';
import {SafeAreaView} from 'react-native-safe-area-context';
import {TextInputWithLabel, Button} from '../widget';
import {COLORS, FontSize, FontType, FontWeight, matrics} from '../../utility';
import Toast from 'react-native-toast-message';
import {User, listKelas} from '../../realm/User';
import FetchLoading from '../widget/FetchLoading';
import {useAuth} from '../../reducer/AuthContextProvider';
const {useRealm, useQuery} = userContext;

const Login = ({navigation}) => {
  const realm = useRealm();
  const userDB = useQuery(User);
  const kelasDB = useQuery(listKelas);
  const AuthContext = useAuth();
  const [dataForm, setDataForm] = useState({email: '', password: ''});
  const [fetching, setFetching] = useState(false);

  // function login user and get data in DB
  const doLogin = () => {
    setFetching(true);
    console.log('do Login');
    // if (loginDB.length > 0) {
    //     realm.write(() => {
    //         realm.delete(loginDB);
    //     });
    // }
    // realm.write(() => {
    //     realm.delete(kelasDB);
    //   });
    if (userDB.length > 0) {
      userDB.map(async user => {
        console.log('user: ' + JSON.stringify(user));
        if (
          user.email === dataForm.email &&
          user.password === dataForm.password
        ) {
          await AuthContext.login(user);
          Toast.show({
            type: 'success',
            position: 'bottom',
            text1: 'Login Berhasil',
            visibilityTime: 1000,
          });
          setTimeout(() => {
            setFetching(false);
            navigation.replace('MenuTab');
          }, 1000);
        } else {
          setFetching(false);
          Toast.show({
            type: 'error',
            position: 'bottom',
            text1: 'Login Gagal',
            visibilityTime: 1000,
            text2: 'Email atau Password tidak ada',
          });
        }
      });
    } else {
      setFetching(false);
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: 'Login Gagal',
        visibilityTime: 1000,
        text2: 'Email atau Password tidak ada',
      });
    }
  };
  return (
    <SafeAreaView style={{flex: 1, alignItems: 'center'}}>
      <View style={{flex: 1}}>
        <View style={{height: matrics.height * 0.15}} />
        <Text
          style={{
            fontFamily: FontType.poppinsSemiBold,
            fontSize: FontSize.font22,
            color: COLORS.green,
          }}>
          Login
        </Text>
      </View>
      <View style={{flex: 2, width: matrics.width * 0.8}}>
        <TextInputWithLabel
          label={'Email'}
          placeholder={'Masukkan Email'}
          placeholderTextColor={COLORS.grey}
          style={{width: '100%'}}
          value={dataForm.email}
          keyboardType={'email-address'}
          onChangeText={email => setDataForm({...dataForm, email})}
        />
        <View style={{height: 10}} />
        <TextInputWithLabel
          label={'Password'}
          placeholder={'Masukkan Password'}
          placeholderTextColor={COLORS.grey}
          style={{width: '100%'}}
          value={dataForm.password}
          secureTextEntry={true}
          onChangeText={password => setDataForm({...dataForm, password})}
        />
        <View style={{height: 20}} />
        <Button
          label={'Login'}
          style={{minWidth: 150}}
          disabled={
            dataForm.email != '' && dataForm.password != '' ? false : true
          }
          onPress={doLogin}
          
        />
      </View>
      <View style={{height: 20}} />
      <Text style={{textAlign: 'center', color: COLORS.grey}}>
        Belum punya akun?{' '}
        <Text
          onPress={() => {
            navigation.navigate('Register');
          }}
          style={{
            color: COLORS.green,
            fontWeight: FontWeight.semi,
            fontSize: FontSize.font14,
          }}>
          Daftar
        </Text>
      </Text>
      <View style={{height: 20}} />
      <FetchLoading visible={fetching} />
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({});
