import {StyleSheet, Text, View, KeyboardAvoidingView} from 'react-native';
import React, {useState} from 'react';
import {userContext} from '../../realm';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Button, DropdownWithLabel, TextInputWithLabel} from '../widget';
import {COLORS, FontSize, FontType, matrics} from '../../utility';
import Toast from 'react-native-toast-message';
import 'react-native-get-random-values';
import {nanoid} from 'nanoid';
import {User} from '../../realm/User';
import FetchLoading from '../widget/FetchLoading';
import {HelperText} from 'react-native-paper';
const {useRealm, useQuery} = userContext;

const initdata = {
  name: '',
  email: '',
  role: '',
  password: '',
  password2: '',
};

const dataDropdown = [
  {label: 'Siswa', value: 'Siswa'},
  {label: 'Guru', value: 'Guru'},
  {label: 'Admin', value: 'Admin'},
];

// function register user and post data in DB
const Register = ({navigation}) => {
  const realm = useRealm();
  const userDB = useQuery(User);
  const [dataForm, setDataForm] = useState(initdata);
  const [fetching, setFetching] = useState(false);
  const doRegister = async () => {
    setFetching(true);
    console.log('do Login');
    if (dataForm.password !== dataForm.password2) {
      setFetching(false);
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: 'Register Failed',
        text2: 'Password must be same',
        visibilityTime: 1000,
      });
    } else {
      let temp = {
        _id: nanoid(),
        ...dataForm,
      };
      console.log('temp', temp);
      if (userDB.length === 0) {
        realm.write(() => {
          realm.create('User', temp);
        });
        Toast.show({
          type: 'success',
          position: 'bottom',
          text1: 'Login Berhasil',
          visibilityTime: 1000,
        });
        setTimeout(() => {
          setFetching(false);
          navigation.popToTop();
        }, 2000);
      } else {
        userDB.map(user => {
          console.log('user', JSON.stringify(user));
          if (user.email === dataForm.email) {
            setFetching(false);
            Toast.show({
              type: 'error',
              position: 'bottom',
              text1: 'Register Failed',
              text2: 'Email already registered',
              visibilityTime: 1000,
            });
          } else {
            realm.write(() => {
              realm.create('User', temp);
            });
            Toast.show({
              type: 'success',
              position: 'bottom',
              text1: 'Login Berhasil',
              visibilityTime: 1000,
            });
            setTimeout(() => {
              setFetching(false);
              navigation.popToTop();
            }, 2000);
          }
        });
      }
    }
  };

  //function to display error message when password not match
  const hasErrors = () => {
    return dataForm.password2 && dataForm.password !== dataForm.password2;
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      style={{flex: 1, alignItems: 'center'}}>
      <View style={{flex: 1}}>
        <View style={{height: matrics.height * 0.15}} />
        <Text
          style={{
            fontFamily: FontType.poppinsSemiBold,
            fontSize: FontSize.font22,
            color: COLORS.green,
          }}>
          Register
        </Text>
      </View>
      <View style={{width: matrics.width * 0.8}}>
        <TextInputWithLabel
          label={'Nama'}
          placeholder={'Masukkan nama'}
          placeholderTextColor={COLORS.grayText}
          style={{width: '100%'}}
          value={dataForm.name}
          onChangeText={name => setDataForm({...dataForm, name})}
        />
        <View style={{height: 10}} />
        <TextInputWithLabel
          label={'Email'}
          placeholder={'Masukkan email'}
          placeholderTextColor={COLORS.grayText}
          style={{width: '100%'}}
          keyboardType={'email-address'}
          value={dataForm.email}
          onChangeText={email => setDataForm({...dataForm, email})}
        />
        <View style={{height: 10}} />
        <DropdownWithLabel
          label={'Role'}
          styleDropdown={{width: '100%'}}
          style={{width: '100%'}}
          data={dataDropdown}
          value={dataForm.role}
          setValue={role => setDataForm({...dataForm, role})}
        />
        <View style={{height: 10}} />
        <TextInputWithLabel
          label={'Password'}
          placeholder={'Masukkan password'}
          placeholderTextColor={COLORS.grayText}
          style={{width: '100%'}}
          secureTextEntry={true}
          value={dataForm.password}
          onChangeText={password => setDataForm({...dataForm, password})}
        />
        <View style={{height: 10}} />
        <TextInputWithLabel
          label={'Konfirmasi Password'}
          placeholder={'Ulangi password'}
          placeholderTextColor={COLORS.grayText}
          style={{width: '100%'}}
          secureTextEntry={true}
          value={dataForm.password2}
          onChangeText={password2 => setDataForm({...dataForm, password2})}
        />
        <HelperText type="error" visible={hasErrors()}>
          {'Password must be same'}
        </HelperText>
        <View style={{height: 20}} />
        <Button
          label={'Daftar Sekarang'}
          style={{minWidth: 150}}
          disabled={
            dataForm.email != '' &&
            dataForm.password != '' &&
            dataForm.telp != '' &&
            dataForm.password2 != '' &&
            dataForm.nama != ''
              ? false
              : true
          }
          onPress={doRegister}
        />
        <View style={{height: 20}} />
      </View>
      <FetchLoading visible={fetching} />
    </KeyboardAvoidingView>
  );
};

export default Register;

const styles = StyleSheet.create({});
