import {Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Header} from '../widget';
import {COLORS, FontSize, FontType, matrics} from '../../utility';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useAuth} from '../../reducer/AuthContextProvider';

const Profile = ({route, navigation}) => {
  const AuthContext = useAuth();

  //function to login user and delete temporary login data in app
  const handleLogout = () => {
    AuthContext.logout();
    navigation.popToTop();
  };
  return (
    <View style={{flex: 1}}>
      <Header screen={route.name} />
      <View style={styles.container}>
        {AuthContext.user.role !== 'Siswa' && (
          <Pressable onPress={() => navigation.navigate('CreateClassForm')}>
            <View style={styles.button}>
              <MaterialCommunityIcons
                name={'google-classroom'}
                size={18}
                color={COLORS.white}
              />
              <View style={{width: 10}} />
              <Text style={styles.textButton}>{'Isi Kelas Baru'}</Text>
            </View>
          </Pressable>
        )}
        <Pressable onPress={handleLogout}>
          <View style={styles.button}>
            <MaterialCommunityIcons
              name={'logout'}
              size={18}
              color={COLORS.white}
            />
            <View style={{width: 10}} />
            <Text style={styles.textButton}>{'Logout'}</Text>
          </View>
        </Pressable>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
    alignItems: 'center',
  },
  button: {
    flexDirection: 'row',
    width: matrics.screenWidth - 20,
    height: 50,
    backgroundColor: COLORS.blue,
    borderRadius: 10,
    alignItems: 'center',
    paddingHorizontal: matrics.width * 0.05,
    marginTop: 15,
  },
  textButton: {
    fontFamily: FontType.robotoMedium,
    fontSize: FontSize.font14,
    color: COLORS.white,
  },
});
