import {
  Pressable,
  ScrollView,
  StyleSheet,
  View,
  FlatList,
  RefreshControl,
  Text,
} from 'react-native';
import React, {useState} from 'react';
import {CardLearning, Header} from '../widget';
import {COLORS, FontSize, FontType, matrics} from '../../utility';
import {useAuth} from '../../reducer/AuthContextProvider';
import {User, listKelas} from '../../realm/User';
import {userContext} from '../../realm';
import FetchLoading from '../widget/FetchLoading';
import Toast from 'react-native-toast-message';
const {useRealm, useQuery} = userContext;

const Home = ({route, navigation}) => {
  const AuthContext = useAuth();
  const [refreshing, setRefreshing] = useState(false);
  const [visible, setVisible] = useState(false);
  const userDB = useQuery(User);
  const kelasDB = useQuery(listKelas);
  const resultKelas = kelasDB.filter(
    item => item.guruId === AuthContext.user._id,
  );
  const realm = useRealm();
  const [fetching, setFetching] = useState(false);
  // console.log(JSON.stringify(kelasDB), 'kelasDB')
  // console.log(JSON.stringify(resultKelas), 'resultKelas')

  //function refresh api list
  function handleRefresh() {
    // setRefreshing(true);
  }

  // function delete list classroom
  const doDelete = value => {
    console.log('value', value);
    setFetching(true);
    realm.write(() => {
      realm.delete(value);
    });
    Toast.show({
      type: 'success',
      position: 'bottom',
      text1: 'Delete Berhasil',
      visibilityTime: 1000,
    });
    setTimeout(() => {
      setFetching(false);
    }, 1000);
    hideDialog();
  };

  //function hide dialog delete
  const hideDialog = () => {
    setVisible(!visible);
  };

  return (
    <View style={{flex: 1}}>
      <Header screen={route.name} />
      <View style={styles.container}>
        {AuthContext.user.role == 'Guru' && resultKelas.length === 0 && (
          <View style={{alignItems: 'center'}}>
            <View style={{height: matrics.height * 0.3}} />
            <Text style={styles.titleCreateClass}>{'Your class is empty'}</Text>
            <View style={{height: 10}} />
            <Pressable
              onPress={() => navigation.navigate('CreateClassForm')}
              style={styles.buttonCreateClass}>
              <Text style={styles.textCreateClass}>
                {'Create ur class now'}
              </Text>
            </Pressable>
          </View>
        )}
        {AuthContext.user.role == 'Guru' && resultKelas.length > 0 && (
          <FlatList
            keyExtractor={(item, index) => `${index}`}
            data={resultKelas}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
                colors={[COLORS.gold, COLORS.white]}
              />
            }
            contentContainerStyle={{paddingHorizontal: matrics.width * 0.05}}
            style={{marginTop: 20}}
            onEndReachedThreshold={0.5}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
            // pagingEnabled={true}
            renderItem={({item, index}) => {
              // console.log('resultKelas', JSON.stringify( resultKelas ));
              // onPress={() =>
              //   navigation.navigate({
              //     name: 'CreateClassForm',
              //     params: {
              //       paramsListKelas: item,
              //     },
              //   })
              // }
              return (
                <CardLearning
                  hideDialog={hideDialog}
                  visible={visible}
                  doDelete={doDelete}
                  item={item}
                  index={index}
                  array={resultKelas}
                  pressCard={() =>
                    navigation.navigate({
                      name: 'DetailClass',
                      params: {
                        paramsDetail: item,
                      },
                    })
                  }
                />
              );
            }}
          />
        )}
        {AuthContext.user.role == 'Siswa' && kelasDB.length > 0 && (
          <FlatList
            keyExtractor={(item, index) => `${index}`}
            data={kelasDB}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
                colors={[COLORS.gold, COLORS.white]}
              />
            }
            contentContainerStyle={{paddingHorizontal: matrics.width * 0.05}}
            style={{marginTop: 20}}
            onEndReachedThreshold={0.5}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
            // pagingEnabled={true}
            renderItem={({item, index}) => {
              // onPress={() =>
              //   navigation.navigate({
              //     name: 'DetailClass',
              //     params: {
              //       paramsDetail: item,
              //     },
              //   })
              // }
              return (
                <CardLearning
                  hideDialog={hideDialog}
                  visible={visible}
                  doDelete={doDelete}
                  item={item}
                  index={index}
                  array={kelasDB}
                  pressCard={() =>
                    navigation.navigate({
                      name: 'DetailClass',
                      params: {
                        paramsDetail: item,
                      },
                    })
                  }
                />
              );
            }}
          />
        )}

        {AuthContext.user.role == 'Admin' && kelasDB.length > 0 && (
          <FlatList
            keyExtractor={(item, index) => `${index}`}
            data={kelasDB}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
                colors={[COLORS.gold, COLORS.white]}
              />
            }
            contentContainerStyle={{paddingHorizontal: matrics.width * 0.05}}
            style={{marginTop: 20}}
            onEndReachedThreshold={0.5}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
            // pagingEnabled={true}
            renderItem={({item, index}) => {
              // onPress={() =>
              //   navigation.navigate({
              //     name: 'CreateClassForm',
              //     params: {
              //       paramsListKelas: item,
              //       admin: true,
              //     },
              //   })
              // }
              return (
                <CardLearning
                  hideDialog={hideDialog}
                  visible={visible}
                  doDelete={doDelete}
                  item={item}
                  index={index}
                  array={kelasDB}
                  pressCard={() =>
                    navigation.navigate({
                      name: 'DetailClass',
                      params: {
                        paramsDetail: item,
                      },
                    })
                  }
                />
              );
            }}
          />
        )}

        {AuthContext.user.role === 'Admin' && kelasDB.length === 0 && (
          <View style={{alignItems: 'center'}}>
            <View style={{height: matrics.height * 0.3}} />
            <Text style={styles.titleCreateClass}>{'List class is empty'}</Text>
            <View style={{height: 10}} />
            <Pressable
              onPress={() => navigation.navigate('CreateClassForm')}
              style={styles.buttonCreateClass}>
              <Text style={styles.textCreateClass}>{'Create a class'}</Text>
            </Pressable>
          </View>
        )}
      </View>
      <FetchLoading visible={fetching} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
    alignItems: 'center',
  },
  buttonCreateClass: {
    width: matrics.width * 0.5,
    height: matrics.height * 0.05,
    backgroundColor: COLORS.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  textCreateClass: {
    color: COLORS.white,
    fontFamily: FontType.robotoRegular,
    fontSize: FontSize.font12,
  },
  titleCreateClass: {
    color: COLORS.black,
    fontFamily: FontType.poppinsRegular,
    fontSize: FontSize.font14,
  },
});
