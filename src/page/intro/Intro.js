import {Image, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import AppIntroSlider from 'react-native-app-intro-slider';
import {COLORS, FontSize, FontType, FontWeight, matrics} from '../../utility';
import {useAuth} from '../../reducer/AuthContextProvider';
import {Button} from '../widget';

const slides = [
  {
    key: 'one',
    title: 'Study Time',
    text: 'Study anytime',
    image: require('../../assets/images/intro1.jpg'),
    backgroundColor: '#59b2ab',
  },
  {
    key: 'two',
    title: 'Learning Fast',
    text: 'Learn with many method and choose ur teacher',
    image: require('../../assets/images/intro2.jpg'),
    backgroundColor: '#febe29',
  },
  {
    key: 'three',
    title: 'Easy to Learn',
    text: 'Easy learn with Profesional Teachers',
    image: require('../../assets/images/intro3.jpg'),
    backgroundColor: '#22bcb5',
  },
];

const Intro = ({navigation}) => {
  const [showRealApp, setShowRealApp] = useState(false);
  const AuthContext = useAuth();

  const _renderItem = ({item}) => {
    console.log(item.image);
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.white,
        }}>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            resizeMode="contain"
            source={item.image}
            style={{height: matrics.height * 0.6, width: matrics.width * 0.6}}
          />
        </View>
        <View style={{position: 'absolute', top: 40, left: 0, right: 0}}>
          <Text
            style={{
              color: COLORS.gold,
              fontFamily: FontType.robotoBold,
              fontSize: FontSize.font28,
              textAlign: 'center',
            }}>
            {item.title}
          </Text>
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 100,
            left: 0,
            right: 0,
            paddingHorizontal: 20,
          }}>
          <Text
            style={{
              color: COLORS.gold,
              fontFamily: FontType.robotoBold,
              fontSize: FontSize.font28,
              textAlign: 'center',
            }}>
            {item.text}
          </Text>
        </View>
      </View>
    );
  };

  const _renderNextButton = () => {
    return (
      <View style={[styles.container, {backgroundColor: COLORS.green}]}>
        <Text style={[styles.styleLabel, {color: COLORS.white}]}>{'Next'}</Text>
      </View>
    );
  };

  const _renderDoneButton = () => {
    return (
      <View style={[styles.container, {backgroundColor: COLORS.green}]}>
        <Text style={[styles.styleLabel, {color: COLORS.white}]}>{'Continue'}</Text>
      </View>
    );
  };
  return (
    <AppIntroSlider
      data={slides}
      renderItem={_renderItem}
      renderDoneButton={_renderDoneButton}
      renderNextButton={_renderNextButton}
      doneLabel={'Continue'}
      nextLabel={'Next'}
      bottomButton={true}
      activeDotStyle={{
        backgroundColor: COLORS.blue,
        width: 20,
        height: 10,
      }}
      dotStyle={{backgroundColor: COLORS.white}}
      onDone={() => {
        if (AuthContext.user === null) {
          navigation.navigate('Login');
        } else {
          navigation.navigate('MenuTab');
        }
        // navigation.navigate('MenuTab')
      }}
      // renderDoneButton={this._renderDoneButton}
      // renderNextButton={this._renderNextButton}
    />
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingVertical: 10,
    backgroundColor: COLORS.gold,
    borderRadius: 10,
    alignItems: 'center',
  },
  styleLabel: {fontWeight: FontWeight.semi, fontSize: FontSize.font14},
});
