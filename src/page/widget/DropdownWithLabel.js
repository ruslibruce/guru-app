import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useState } from 'react';
import { COLORS, FontSize, FontType } from '../../utility';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Dropdown, MultiSelect } from 'react-native-element-dropdown';

const DropdownWithLabel = ({
  label,
  data,
  style,
  setValue,
  styleLabel,
  inputText,
  styleDropdown,
  multiline = false,
  value,
  multiSelect,
  disable = false,
}) => {
  const renderItem = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {/* {item.value === value && (
              <AntDesign
                style={styles.icon}
                color="black"
                name="Safety"
                size={20}
              />
            )} */}
      </View>
    );
  };

  return (
    <View style={style}>
      <Text style={[styles.label, styleLabel]}>{label}</Text>
      <View style={{ height: 5 }} />
      {multiSelect ? (
        <MultiSelect
          style={[styles.dropdown, styleDropdown]}
          placeholderStyle={styles.placeholderStyle}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          iconStyle={styles.iconStyle}
          data={data}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          placeholder="Select item"
          searchPlaceholder="Search..."
          value={value}
          disable={disable}
          onChange={item => {
            multiSelect ? setValue(item) : setValue(item.value);
          }}
          renderSelectedItem={(item, unSelect) => (
            <TouchableOpacity onPress={() => unSelect && unSelect(item)}>
              <View style={styles.selectedStyle}>
                <Text style={styles.textSelectedStyle}>{item.label}</Text>
              </View>
            </TouchableOpacity>
          )}
          // renderLeftIcon={() => (
          //     <AntDesign style={styles.icon} color="black" name="Safety" size={20} />
          // )}
          renderItem={renderItem}
        />
      ) : (
        <Dropdown
          style={[styles.dropdown, styleDropdown]}
          placeholderStyle={styles.placeholderStyle}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          iconStyle={styles.iconStyle}
          data={data}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          placeholder="Select item"
          searchPlaceholder="Search..."
          value={value}
          onChange={item => {
            setValue(item.value);
          }}
          // renderLeftIcon={() => (
          //     <AntDesign style={styles.icon} color="black" name="Safety" size={20} />
          // )}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};

export default DropdownWithLabel;

const styles = StyleSheet.create({
  label: {
    fontFamily: FontType.poppinsMedium,
    textAlign: 'left',
    fontSize: FontSize.font14,
    color: COLORS.green,
  },
  inputText: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: FontSize.font12,
    height: 40,
    lineHeight: 15,
  },
  dropdown: {
    height: 40,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
  selectedStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 14,
    backgroundColor: 'white',
    shadowColor: '#000',
    marginTop: 8,
    marginRight: 12,
    paddingHorizontal: 12,
    paddingVertical: 8,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  textSelectedStyle: {
    marginRight: 5,
    fontSize: 16,
  },
});
