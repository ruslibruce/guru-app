import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FontSize, FontType, matrics} from '../../utility';
import {useAuth} from '../../reducer/AuthContextProvider';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useNavigation} from '@react-navigation/native';

const Header = props => {
  const AuthContext = useAuth();
  const navigation = useNavigation();
  React.useEffect(() => {
    console.log('props', props);
  }, []);

  return (
    <View style={styles.container}>
      {props.screen === 'Home' && (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{width: matrics.width * 0.05}} />
          <View style={styles.formAvatar}>
            <Image
              style={styles.avatarImage}
              source={require('../../assets/images/avatar_male.png')}
            />
          </View>
          <View style={{width: 10}} />
          <Text style={styles.textHeader}>{`Hi ${AuthContext.user.name}`}</Text>
        </View>
      )}

      {props.screen === 'Profile' && (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{width: matrics.width * 0.05}} />
          <Text style={styles.textHeader}>{`${props.screen}`}</Text>
        </View>
      )}

      {props.screen === 'CreateClassForm' && (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{width: matrics.width * 0.05}} />
          <FontAwesome5
            onPress={() => navigation.goBack()}
            name="arrow-left"
            size={20}
            color={COLORS.white}
          />
          <View style={{width: 10}} />
          <Text style={styles.textHeader}>{`Form Classroom`}</Text>
        </View>
      )}

      {props.screen === 'DetailClass' && (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{width: matrics.width * 0.05}} />
          <FontAwesome5
            onPress={() => navigation.goBack()}
            name="arrow-left"
            size={20}
            color={COLORS.white}
          />
          <View style={{width: 10}} />
          <Text style={styles.textHeader}>{`Detail Classroom`}</Text>
        </View>
      )}
      <View style={{height: 10}} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: matrics.height * 0.08,
    backgroundColor: COLORS.green,
    justifyContent: 'flex-end',
  },
  textHeader: {
    fontFamily: FontType.poppinsRegular,
    fontSize: FontSize.font20,
    color: COLORS.white,
  },
  avatarImage: {
    width: '100%',
    height: '100%',
  },
  formAvatar: {
    width: 30,
    height: 30,
    borderRadius: 30,
    overflow: 'hidden',
  },
});
