import {ScrollView, StyleSheet, View} from 'react-native';
import React from 'react';
import {Avatar, Button, Card, Text, Divider} from 'react-native-paper';
import {COLORS, FontSize, FontType, matrics} from '../../utility';
import {Header} from '../widget';
import {useAuth} from '../../reducer/AuthContextProvider';

const DetailClass = ({navigation, route}) => {
  // console.log('route params', JSON.stringify(route.params.paramsDetail));
  const DetailContent = route.params.paramsDetail;
  const AuthContext = useAuth();

  return (
    <View style={{flex: 1}}>
      <Header screen={route.name} />
      <ScrollView contentContainerStyle={{paddingBottom: 20}}>
        <Card
          style={{
            width: matrics.width * 0.9,
            // height: matrics.height * 0.34,
            alignSelf: 'center',
            marginTop: 20,
          }}>
          <Card.Title
            titleStyle={styles.cardTitle}
            subtitleStyle={styles.subsTitle}
            title={'Classroom Name :'}
            subtitle={DetailContent.className}
          />
          <Card.Cover
            style={{marginHorizontal: 15}}
            source={{uri: DetailContent.image}}
          />
          <Card.Content>
            <View style={{height: 5}} />
            <View style={{height: 5}} />
            <Text style={styles.cardTitle} variant="bodyMedium">
              {'Description :'}
            </Text>
            <View style={{height: 5}} />
            <Text style={styles.subsTitle} variant="titleLarge">
              {DetailContent.deskripsi}
            </Text>
            <View style={{height: 5}} />
            <Text style={styles.cardTitle} variant="bodyMedium">
              {'Teacher :'}
            </Text>
            <View style={{height: 5}} />
            <Text style={styles.subsTitle} variant="titleLarge">
              {'• ' + DetailContent.createdBy}
            </Text>
            <View style={{height: 5}} />
            {DetailContent.listStudent.length > 0 && (
              <>
                <View style={{height: 5}} />
                <Text style={styles.cardTitle} variant="bodyMedium">
                  {'List Student :'}
                </Text>
                <Divider />
                <View style={{height: 5}} />
                {DetailContent.listStudent.map((val, ind) => {
                  return (
                    <>
                      <Text
                        style={styles.subsTitle}
                        numberOfLines={1}
                        variant="bodyMedium">
                        {'• ' + val.name}
                      </Text>
                    </>
                  );
                })}
                <View style={{height: 5}} />
              </>
            )}
          </Card.Content>
          {AuthContext.user.role !== 'Siswa' && (
            <Card.Actions>
              <Button
                onPress={() =>
                  navigation.navigate({
                    name: 'CreateClassForm',
                    params: {
                      paramsListKelas: DetailContent,
                    },
                  })
                }>
                {'Ubah Data Kelas'}
              </Button>
            </Card.Actions>
          )}
        </Card>
      </ScrollView>
    </View>
  );
};

export default DetailClass;

const styles = StyleSheet.create({
  cardTitle: {
    color: COLORS.black,
    fontFamily: FontType.poppinsBold,
    fontSize: FontSize.font16,
  },
  subsTitle: {
    color: COLORS.black,
    fontFamily: FontType.robotoRegular,
    fontSize: FontSize.font16,
  },
});
