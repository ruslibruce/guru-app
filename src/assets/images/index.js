import intro1 from './intro1.jpg'
import intro2 from './intro2.jpg'
import intro3 from './intro3.jpg'
import avatar_male from './avatar_male.jpg'
import noImage from './no-image-icon.jpg'
export { intro1, intro2, intro3, avatar_male, noImage }