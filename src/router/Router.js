import {View, Text, Platform} from 'react-native';
import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {HomeScreen} from '../page/bottomTab';
import {ProfileScreen} from '../page/bottomTab';
import {Intro} from '../page/intro';
import {COLORS, FontType} from '../utility';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Login, Register} from '../page/authPage';
import {useAuth} from '../reducer/AuthContextProvider';
import {CreateClassForm} from '../page/createClass';
import {DetailClass} from '../page/detailClass';

const Tab = createMaterialBottomTabNavigator();
const Stack = createNativeStackNavigator();

const Router = () => {
  const AuthContext = useAuth();
  // console.log('AUTH===>', JSON.stringify(AuthContext));
  function MyTabs(props) {
    // const routeName = getFocusedRouteNameFromRoute(route) ?? 'Feed';
    // console.log('cek props', props);
    return (
      <Tab.Navigator
        initialRouteName="Home"
        activeColor={COLORS.blue}
        inactiveColor={COLORS.white}
        shifting={true}
        sceneAnimationEnabled={false}
        barStyle={{
          backgroundColor: COLORS.green,
          marginBottom: Platform.OS === 'ios' ? 10 : 0,
          marginTop: 5,
          paddingHorizontal: 12,
          paddingTop: 0,
          height: 60,
          marginHorizontal: 5,
          overflow: 'hidden',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Tab.Screen
          options={{
            tabBarLabel: (
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: FontType.robotoRegular,
                  textAlign: 'center',
                }}>
                {'Home'}
              </Text>
            ),
            tabBarIcon: ({color}) => (
              <FontAwesome5 name="home" color={color} size={25} />
            ),
          }}
          name="Home"
          component={HomeScreen}
        />
        <Tab.Screen
          options={{
            tabBarLabel: (
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: FontType.robotoRegular,
                  textAlign: 'center',
                }}>
                {'Profile'}
              </Text>
            ),
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons name="account" color={color} size={25} />
            ),
          }}
          name="Profile"
          component={ProfileScreen}
        />
      </Tab.Navigator>
    );
  }
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MenuTab"
        component={MyTabs}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CreateClassForm"
        component={CreateClassForm}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DetailClass"
        component={DetailClass}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
