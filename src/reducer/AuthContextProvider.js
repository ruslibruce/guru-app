import { View, Text } from 'react-native'
import React, { createContext, useContext, useEffect, useMemo, useReducer } from 'react'
import DeviceInfo from 'react-native-device-info'
import moment from 'moment'
import { createAction, wait } from '../utility'
import { UserLogin, User, Siswa, listKelas } from '../realm/User'
import { userContext } from '../realm'
const { useRealm, useQuery } = userContext;

const AuthContext = createContext();

const initialState = {
    user: null,
    isLoading: true,
    isLogin: false,
    deviceOs: DeviceInfo.getSystemName(),
    deviceType: DeviceInfo.getBrand(),
    osVersion: DeviceInfo.getSystemVersion(),
    device_info: null,
}

const authReducer = (state, action) => {
    switch (action.type) {
        case 'SET_USER':
            return {
                ...state,
                user: action.payload,
            }
        case 'REMOVE_USER':
            return {
                ...state,
                user: undefined,
            }
        case "SET_DATA":
            return {
                ...state,
                ...action.payload
            }
        case 'SET_LOADING':
            return {
                ...state,
                isLoading: action.payload,
            }
        case 'SET_DEVICE':
            return {
                ...state,
                device_info: action.payload,
            }


        default:
            return state;
    }
}

const AuthContextProvider = ({ children }) => {
    const [authState, dispatch] = useReducer(authReducer, initialState);
    const realm = useRealm();
    const loginDB = useQuery(UserLogin);
    const userDB = useQuery(User);
    const siswaDB = useQuery(Siswa);
    const listKelasDB = useQuery(listKelas);
    // console.log('cek realm db login', JSON.stringify(loginDB));
    // console.log('cek realm db user', JSON.stringify(userDB));
    // console.log('cek realm db kelas', JSON.stringify(listKelasDB));
    // console.log('cek realm db siswa', JSON.stringify(siswaDB));

    useEffect(() => {
        var devices = {}
        DeviceInfo.getUserAgent().then((userAgent) => {
          devices = {
            globalId: global.uniqueId,
            deviceUserAgent: userAgent,
            deviceType: DeviceInfo.getDeviceType(),
            macAddress: DeviceInfo.getMacAddressSync(),
            applicationName: DeviceInfo.getApplicationName(),
            installerPackageName : DeviceInfo.getInstallerPackageNameSync(),
            deviceName: DeviceInfo.getDeviceNameSync(),
            deviceId: DeviceInfo.getDeviceId(),
            versionCodeApp: DeviceInfo.getBuildNumber(),
            locationStatus: DeviceInfo.isLocationEnabledSync(),
            providerStatus: DeviceInfo.getCarrierSync(),
            locationProvider: DeviceInfo.getAvailableLocationProvidersSync(),
            manufacture: DeviceInfo.getManufacturerSync(),
            OSVersion: DeviceInfo.getSystemVersion(),
            productName: DeviceInfo.getProductSync(),
            firstInstallTime: moment(DeviceInfo.getFirstInstallTimeSync()).format(
              'DD MMM YYYY HH:mm',
            ),
            systemName: DeviceInfo.getSystemName(),
            phoneBrand: DeviceInfo.getBrand(),
            bundleId: DeviceInfo.getBundleId(),
            version: DeviceInfo.getVersion(),
            deviceIsEmulator: isEmulator,
          }
        });
        dispatch(createAction('SET_LOADING', true))
        wait(2000).then(() => {
            dispatch(createAction('SET_DEVICE', devices));
            if (loginDB.length > 0) {
                var tempObject = [...loginDB]
                var data = { ...authState, user: tempObject.pop(), isLogin: true, isLoading: false }
                dispatch(createAction('SET_DATA', data));
            } else {
                var data = { ...initialState, user: null, isLogin: false, isLoading: false }
                dispatch(createAction('SET_DATA', data));
                dispatch(createAction('SET_LOADING', false));
            }
        })
    }, [])


    const updateAuth = useMemo(() => ({
        //context login post to DB
        login: async data => {
            // let temp = {
            //     ...data,
            //     listKelas: kelasDB,
            // }
            var dataInitial = { ...authState, user: data, isLogin: true }
            let dataLogin = JSON.stringify(data);
            console.log("dataLogin", dataLogin)
            realm.write(() => {
                realm.create('UserLogin', data);
            });
            dispatch(createAction('SET_DATA', dataInitial))
            dispatch(createAction('SET_LOADING', false))
        },
        //context set user temp in apps
        setUser: async data => {
            dispatch(createAction('SET_USER', { user: data }));
        },
        //context logout delete temp user in apps
        logout: async () => {
            dispatch(createAction('SET_LOADING', true));
            realm.write(() => {
                realm.delete(loginDB);
            });
            var data = { ...authState, user: null, isLogin: false, loading: false }
            dispatch(createAction('SET_DATA', data));
            dispatch(createAction('SET_LOADING', false));
        },
    }))

    return (
        <AuthContext.Provider value={{ ...authState, ...updateAuth }}>
            {children}
        </AuthContext.Provider>
    )
}

const useAuth = () => useContext(AuthContext);

export { AuthContextProvider, useAuth }