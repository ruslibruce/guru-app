import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Toast from 'react-native-toast-message';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { AuthContextProvider } from './reducer';
import { NavigationContainer } from '@react-navigation/native';
import { Router } from './router';
import { COLORS } from './utility';
import { FontType, FontSize } from './utility';
import { userContext } from './realm';
import Realm from 'realm';
import { DefaultTheme, PaperProvider } from 'react-native-paper';

// function locate db realm
Realm.open({}).then(realm => {
  console.log("Realm is located at: " + realm.path);
});

//function get default theme for bottom navigation
const myNavigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    notification: 'rgba(255, 255, 255, 0.5)',
    secondaryContainer: 'transparent',
  },
};

// function call toast message
const toastConfig = {
  success: (props) => {
    // console.log(props)
    return <View style={{ minWidth: 197, alignSelf: "center", borderColor: COLORS.blue, backgroundColor: COLORS.blue, borderWidth: 1, borderRadius: 15, paddingVertical: 10, paddingHorizontal: 20, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ fontFamily: FontType.poppinsRegular, color: "#FFFFFF", fontSize: FontSize.font14 }}>{props.text1}</Text>
    </View>
  },
  successTop: (props) => {
    // console.log(props)
    return <View style={{ minWidth: 197, alignSelf: "center", borderColor: COLORS.blue, backgroundColor: COLORS.blue, borderWidth: 1, borderRadius: 15, paddingVertical: 10, paddingHorizontal: 20, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ fontFamily: FontType.poppinsRegular, color: COLORS.white, fontSize: FontSize.font14 }}>{props.text1}</Text>
    </View>
  },
  error: (props) => {
    // console.log(props)
    return <View style={{ minWidth: 197, alignSelf: "center", borderColor: COLORS.red, backgroundColor: COLORS.red, borderWidth: 1, borderRadius: 15, paddingVertical: 10, paddingHorizontal: 20, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ fontFamily: FontType.poppinsRegular, color: COLORS.white, fontSize: FontSize.font14 }}>{props.text1}</Text>
      <Text style={{ fontFamily: FontType.robotoRegular, color: COLORS.white, fontSize: FontSize.font12 }}>{props.text2}</Text>
    </View>
  },
};

// make realm provider available
const { RealmProvider, useRealm, useObject, useQuery } = userContext

const App = () => {
  return (
    <SafeAreaProvider>
      <RealmProvider>
        <AuthContextProvider>
          <NavigationContainer>
            <PaperProvider theme={myNavigationTheme}>
              <Router />
            </PaperProvider>
          </NavigationContainer>
        </AuthContextProvider>
      </RealmProvider>
      <Toast config={toastConfig} />
    </SafeAreaProvider>
  )
}

export default App

const styles = StyleSheet.create({})