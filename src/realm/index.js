import { UserLogin, User, Siswa, listKelas } from './User';
import { createRealmContext } from '@realm/react';

// Create a configuration object
export const userContext = createRealmContext({
  schema: [UserLogin, User, Siswa, listKelas],
  schemaVersion: 1,
});