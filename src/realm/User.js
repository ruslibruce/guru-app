import Realm from 'realm';
//Database Offline

// Define your object model
export class UserLogin extends Realm.Object {
  static schema = {
    name: 'UserLogin',
    properties: {
      _id: 'string',
      name: 'string',
      email: 'string',
      role: 'string',
      password: 'string',
      password2: 'string',
    },
    primaryKey: '_id',
  };
}

// Define your object model
export class User extends Realm.Object {
  static schema = {
    name: 'User',
    properties: {
      _id: 'string',
      name: 'string',
      email: 'string',
      role: 'string',
      password: 'string',
      password2: 'string',
    },
    primaryKey: '_id',
  };
}

// Define your object model
export class listKelas extends Realm.Object {
  static schema = {
    name: 'listKelas',
    properties: {
      _id: 'string',
      guruId: 'string',
      className: 'string',
      deskripsi: 'string',
      image: 'string',
      createdBy: 'string',
      listStudent: 'Siswa[]',
    },
    primaryKey: '_id',
  };
}
//listMasukKelas
//Kelas

// Define your object model
export class Siswa extends Realm.Object {
  static schema = {
    name: 'Siswa',
    properties: {
      _id: 'string',
      siswaId: 'string',
      name: 'string',
      kelas: 'string',
    },
    primaryKey: '_id',
  };
}